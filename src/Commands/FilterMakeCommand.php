<?php

namespace Xbhub\Filter\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class FilterMakeCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:filter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new filter class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Filter';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/stubs/filter.stub';
    }

    /**
     * set default namespace
     *
     * @return void
     */
    protected function rootNamespace()
    {
        if($module = $this->option('module')) {
            $namespace = $this->laravel['config']->get('modules.namespace');
            return $namespace. DIRECTORY_SEPARATOR .Str::studly($module);
        }
        return $this->laravel->getNamespace();
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {

        return $rootNamespace . '\Http\Filters';
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        $basePath = $this->option('module')
                        ? module_path($this->option('module'))
                        : $this->laravel['path'];
        return $basePath.'/'.str_replace('\\', '/', $name).'.php';
    }

    /**
     * command options
     *
     * @return void
     */
    public function getOptions()
    {
        return [
            ['module','m',InputOption::VALUE_OPTIONAL,'Module Name',null]
        ];
    }
}
